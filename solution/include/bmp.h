#ifndef BMP_RW_H
#define BMP_RW_H

#include  <stdio.h>

#include "image.h"

/*  deserializer   */
enum read_status  {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_SIGNATURE,
	READ_INVALID_OFFSET,
	READ_INVALID_DIB_SIZE,
	READ_INVALID_PLANES,
	READ_UNSUPPORTED,
	READ_INVALID_PIXELS,
	READ_INVALID_IMAGE_SIZE,
	READ_INVALID_FILE_SIZE,
	READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
	WRITE_OK = 0,
	WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img );

const char* read_err_msg(enum read_status s);

#endif
