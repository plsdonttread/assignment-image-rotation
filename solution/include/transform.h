#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"

struct image rotate(const struct image src);

#endif
