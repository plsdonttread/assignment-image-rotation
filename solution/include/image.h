#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct  __attribute__((packed)) pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_init(struct image* img, const uint64_t width, const uint64_t height);
struct image image_new(const uint64_t width, const uint64_t height);
void image_free(struct image image);

struct pixel* pixel_of(const struct image img, const uint64_t x, const uint64_t y);

#endif
