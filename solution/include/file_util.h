#ifndef FILE_UTIL_H
#define FILE_UTIL_H

#include <stdio.h>
#include <errno.h>
#include <string.h>

FILE* open_read_or_exit(const char* path);
FILE* open_write_or_exit(const char* path);

#endif
