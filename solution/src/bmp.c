#include <stdint.h>
#include <stdio.h>

#include "bmp.h"

#define BMP_SIGNATURE 0x4D42
#define BMP_RESERVED 0
#define BMP_DEFAULT_OFFSET 54
#define BMP_BID_SIZE 40
#define BMP_PLANES 1
#define BMP_BITCOUNT 24
#define BMP_COMPRESSION 0

struct __attribute__((packed)) bmp_header 
{
	uint16_t bfType;
	uint32_t  bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t  biHeight;
	uint16_t  biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t  biClrImportant;
};

struct bmp_header bmp_header_default() {
	struct bmp_header h;
	h.bfType = BMP_SIGNATURE;
	h.bfReserved = BMP_RESERVED;
	h.bOffBits = BMP_DEFAULT_OFFSET;
	h.biSize = BMP_BID_SIZE;
	h.biPlanes = BMP_PLANES;
	h.biBitCount = BMP_BITCOUNT;
	h.biCompression = BMP_COMPRESSION;
	h.biXPelsPerMeter = 0;
	h.biYPelsPerMeter = 0;
	h.biClrUsed = 0;
	h.biClrImportant = 0;
	return h;
}

enum read_status check_bmp_header(struct bmp_header const* h) {
	if(h->bfType != BMP_SIGNATURE) return READ_INVALID_SIGNATURE;
	if(h->bOffBits < BMP_DEFAULT_OFFSET) return READ_INVALID_OFFSET;
	if(h->biSize < BMP_BID_SIZE) return READ_INVALID_DIB_SIZE;
	if(h->biPlanes != BMP_PLANES) return READ_INVALID_PLANES;
	if(h->biBitCount != BMP_BITCOUNT) return READ_UNSUPPORTED;
	if(h->biCompression != BMP_COMPRESSION) return READ_UNSUPPORTED;

	if(h->biSizeImage < h->biWidth * h->biHeight * 3) return READ_INVALID_IMAGE_SIZE;
	if(h->biSizeImage + h->bOffBits != h->bfileSize) return READ_INVALID_FILE_SIZE;

	return READ_OK;
}

static uint32_t inline calc_extra_bytes(uint32_t width) {
	return (4 - ((width * 3) % 4)) % 4;
}

enum read_status read_bmp_header( FILE* in, struct bmp_header* bmp ) {
	const size_t header_read_res = fread(bmp, sizeof(struct bmp_header), 1, in);
	if(header_read_res != 1) return READ_INVALID_HEADER;

	const enum read_status header_check_res = check_bmp_header(bmp);
	if(header_check_res != READ_OK) return header_check_res;

	return READ_OK;
}

enum read_status read_bmp_pixels( FILE* in, struct image* img, struct bmp_header const* bmp ) {
	const uint32_t extra_bytes = calc_extra_bytes(img->width);
	fseek(in, bmp->bOffBits, 0);

	for(size_t h = 0; h < img->height; h++) {
		const size_t pixels_read_res = fread(&img->data[img->width*h], sizeof(struct pixel), img->width, in);

		if(pixels_read_res != img->width) {
			return READ_INVALID_PIXELS;
		}

		if(extra_bytes != 0 && fseek(in, extra_bytes, 1) != 0) {
			return READ_INVALID_PIXELS;
		}
	}

	return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
	struct bmp_header bmp;

	const enum read_status read_header_res = read_bmp_header(in, &bmp);
	if(read_header_res != READ_OK) {
		return read_header_res;
	}

	image_init(img, bmp.biWidth, bmp.biHeight);

	return read_bmp_pixels(in, img, &bmp);
}

struct bmp_header bmp_header_create(struct image const* img) {
	struct bmp_header bmp = bmp_header_default();

	bmp.biWidth = img->width;
	bmp.biHeight = img->height;

	const uint32_t extra_bytes = calc_extra_bytes(img->width);

	bmp.biSizeImage = bmp.biWidth*bmp.biHeight*3 + extra_bytes * bmp.biHeight;
	bmp.bfileSize = bmp.biSizeImage + bmp.bOffBits;

	return bmp;
}

enum write_status write_bmp_header( FILE* out, struct image const* img ){
	const struct bmp_header bmp = bmp_header_create(img);

	const size_t res = fwrite(&bmp, sizeof(struct bmp_header), 1, out);
	if(res != 1) {
		return WRITE_ERROR;
	}

	return WRITE_OK;

}

enum write_status write_bmp_pixels( FILE* out, struct image const* img ){

	if(ftell(out) < BMP_DEFAULT_OFFSET) return WRITE_ERROR;

	const uint32_t extra_bytes = calc_extra_bytes(img->width);
	
	char zeroes[4] = {0};
	for(size_t h = 0; h < img->height; h++) {
		const size_t res = fwrite(&img->data[img->width*h], sizeof(struct pixel), img->width, out);

		if(res != img->width) {
			return WRITE_ERROR;
		}

		if(extra_bytes != 0 
			&& fwrite(zeroes, sizeof(char), extra_bytes, out) != extra_bytes) {
			return WRITE_ERROR;
		}
	}

	return WRITE_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){

	const size_t header_write_res = write_bmp_header(out, img);
	if(header_write_res != WRITE_OK) {
		return WRITE_ERROR;
	}

	return write_bmp_pixels(out, img);
}

const char* read_err_msgs[] = {
	[READ_INVALID_HEADER] = "Cannot read header data",
	[READ_INVALID_SIGNATURE] = "Invalid signature",
	[READ_INVALID_OFFSET] = "Invalid payload offset",
	[READ_INVALID_DIB_SIZE] = "Invalid DIB size",
	[READ_INVALID_PLANES] = "Invalid planes number",
	[READ_UNSUPPORTED] = "Unsupported BMP image type",
	[READ_INVALID_PIXELS] = "Cannot read pixels data",
	[READ_INVALID_IMAGE_SIZE] = "Invalid image size in header",
	[READ_INVALID_FILE_SIZE] = "Invalid file size in header",
	[READ_ERROR] = "Some error occured",
};

const char* read_err_msg(enum read_status s) {
	return read_err_msgs[s];
}
