#include <malloc.h>

#include "image.h"

void image_init(struct image* img, const uint64_t width, const uint64_t height) {
	img->width = width;
	img->height = height;
	img->data = malloc(sizeof(struct pixel) * width * height);
}

struct image image_new(const uint64_t width, const uint64_t height) {
	struct image img;
	image_init(&img, width, height);
	return img;
}

void image_free(struct image image) {
  free(image.data);
}

inline struct pixel* pixel_of(const struct image img, const uint64_t x, const uint64_t y) {
	return &img.data[y*img.width+x];
}
