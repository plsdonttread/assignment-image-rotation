#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "file_util.h"
#include "bmp.h"
#include "image.h"

#include "transform.h"

static struct image read_image_from_bmp(const char* path) {
	struct image src_img;

	FILE* input_fp = open_read_or_exit(path);

	enum read_status read_st = from_bmp(input_fp, &src_img);
	if(read_st != READ_OK) {
		fprintf(stderr, "Input file format error: %s.", read_err_msg(read_st));
		image_free(src_img);
		fclose(input_fp);
		exit(1);
	}

	fclose(input_fp);
	return src_img;
}

static void write_image_to_bmp(const struct image* img, const char* path) {
	FILE* output_fp = open_write_or_exit(path);

	enum write_status write_st = to_bmp(output_fp, img);
	if(write_st != WRITE_OK) {
		fprintf(stderr, "Output write error occured.");
		image_free(*img);
		fclose(output_fp);
		exit(1);
	}

	fclose(output_fp);
}

int main(int argc, char** args) {

	char* src_path;
	char* dest_path;

	if(argc != 3 && argc != 1) {
		fprintf(stderr, "Invalid arguments.");
		return 1;
	}
	if(argc == 1) {
		src_path = "input.bmp";
		dest_path = "output.bmp";
	}
	if(argc == 3) {
		src_path = args[1];
		dest_path = args[2];
	}

	const struct image src_img = read_image_from_bmp(src_path);
	const struct image dest_img = rotate(src_img);
	image_free(src_img);

	write_image_to_bmp(&dest_img, dest_path);
	image_free(dest_img);

	return 0;
}
