#include "file_util.h"

#include <stdlib.h>

FILE* open_read_or_exit(const char* path) {
    FILE* fp = fopen(path, "rb");
    if(fp == NULL) {
        fprintf(stderr, "Cannot open file for read: %s.", strerror(errno));
        exit(1);
    }  
    return fp;
}

FILE* open_write_or_exit(const char* path) {
    FILE* fp = fopen(path, "wb");
	if(fp == NULL) {
		fprintf(stderr, "Cannot open file for write: %s.", strerror(errno));
		exit(1);
	}
    return fp;
}
